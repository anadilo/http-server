# HTTP Server

Simple HTTP server.

## Config and start

```
val config = Config(port = 8080, webRoot = "/static")
Server.start(config)
...
Server.stop()
```

### Request mapping

Map to file:

```
Server.mapFile(Mapping("/"), "index.html")
```

Map to method:

```
Server.mapRequest(Mapping("/api/submit", HttpMethod.POST)) {
    httpRequest -> RawResponse("${httpRequest.body?.size ?: 0} bytes received".toByteArray(),
        ContentType.TEXT_PLAIN)
}
```
