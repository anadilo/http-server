package org.andi.com.engine

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.andi.com.AccountController
import org.andi.com.config.Config
import org.andi.com.controller.HttpController
import org.andi.com.controller.RequestMapping
import org.andi.com.file.FileProvider
import org.andi.com.file.RawResponse
import org.andi.com.mapping.Mapping
import org.andi.com.mapping.MappingHandler
import org.andi.com.protocol.HttpRequest
import org.andi.com.protocol.HttpResponse
import org.andi.com.protocol.ResponseStatus
import org.andi.com.socket.HttpSocket
import org.andi.com.util.logger
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.SocketException

object Server {

    private val logger = logger<Server>()

    private var config: Config = Config()
    private val serverSocket: ServerSocket = ServerSocket()

    private lateinit var thread: Thread
    private var running = false

    fun start(cfg: Config? = null) {
        cfg?.let { config = it }

        serverSocket.reuseAddress = true
        serverSocket.bind(InetSocketAddress(config.port))

        if (running) {
            logger.warn("Server already running")
            return
        }

        running = true

        thread = Thread(Runnable { listen() }, "http-server-thread")
        thread.start()

        logger.info("HttpServer started and listening at port ${serverSocket.localPort}...")

        println(AccountController)
    }

    fun stop() {
        running = false
        serverSocket.close()
        thread.join(1_000)

        logger.info("HttpServer stopped")
    }

    private fun listen() {
        while (running) {
            try {
                val httpSocket = HttpSocket(serverSocket.accept())

                GlobalScope.launch {
                    httpSocket.use {
                        try {
                            serve(it)
                        } catch (e: Exception) {
                            logger.error("Error serving request", e)
                            ResponseProcessor.writeResponse(it.writer, HttpResponse(ResponseStatus.STATUS_500))
                        }
                    }
                }
            } catch (se: SocketException) {
                logger.info("Listener stopped")
            } catch (e: Exception) {
                logger.error("Socket error", e)
            }
        }
    }

    private fun serve(httpSocket: HttpSocket) {
        val httpRequest = RequestProcessor.process(httpSocket.reader)

        if (config.logRequests)
            logger.info("$httpRequest")

        ResponseProcessor.process(httpSocket.writer, httpRequest)
    }

    fun mapRequest(mapping: Mapping, action: (HttpRequest) -> RawResponse?) {
        MappingHandler.put(mapping, action)
    }

    fun mapFile(mapping: Mapping, file: String) {
        MappingHandler.put(mapping) { FileProvider.readFile(config.webRoot, file) }
        logger.info("Mapped $file to $mapping")
    }

    fun mapRequest(
            requestMapping: RequestMapping,
            pathPrefix: String,
            action: (HttpRequest) -> RawResponse?) {
        mapRequest(Mapping(pathPrefix.plus(requestMapping.path), requestMapping.method), action)
    }

}