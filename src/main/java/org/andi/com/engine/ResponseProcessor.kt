package org.andi.com.engine

import org.andi.com.mapping.Mapping
import org.andi.com.mapping.MappingHandler
import org.andi.com.protocol.*
import org.andi.com.util.exceptionToString
import java.io.BufferedOutputStream

object ResponseProcessor {

    fun process(writer: BufferedOutputStream, httpRequest: HttpRequest) {
        val requestLine = httpRequest.requestLine
        val httpVersion = requestLine.httpVersion

        val action = MappingHandler.get(Mapping(requestLine.resource, requestLine.method))

        try {
            if (action == null) {
                writeResponse(writer, HttpResponse(ResponseStatus.STATUS_404, httpVersion))
                return
            }

            val rawResponse = action.invoke(httpRequest)
            val httpResponse = HttpResponse(ResponseStatus.STATUS_200, httpVersion, rawResponse)

            writeResponse(writer, httpResponse)

        } catch (e: Exception) {
            writeResponse(writer, HttpResponse(ResponseStatus.STATUS_500, httpVersion, exception = e))
        }
    }

    fun writeResponse(writer: BufferedOutputStream, httpResponse: HttpResponse) {
        writer.write(getStatusLine(httpResponse).toByteArray())
        writer.write(getHeaders(httpResponse).toByteArray())

        httpResponse.rawResponse?.let { writer.write(it.bytes) }
        httpResponse.exception?.let { writer.write(exceptionToString(it).toByteArray()) }

        writer.flush()
    }

    private fun getStatusLine(httpResponse: HttpResponse): String {
        return "${httpResponse.httpVersion} ${httpResponse.responseStatus.toStatusLine()}"
    }

    private fun getHeaders(httpResponse: HttpResponse): String {
        return HttpHeader.DATE.lineCreator?.invoke(httpResponse) +
                HttpHeader.CONTENT_LENGTH.lineCreator?.invoke(httpResponse) +
                HttpHeader.CONTENT_TYPE.lineCreator?.invoke(httpResponse) +
                HttpProtocol.LINE_END
    }

}