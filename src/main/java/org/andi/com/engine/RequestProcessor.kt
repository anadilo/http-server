package org.andi.com.engine

import org.andi.com.protocol.HttpHeader
import org.andi.com.protocol.HttpProtocol
import org.andi.com.protocol.HttpRequest
import java.io.BufferedReader

object RequestProcessor {

    fun process(reader: BufferedReader): HttpRequest {
        val requestLine = HttpProtocol.toRequestLine(reader.readLine())

        val headers = HashMap<String, String>()

        do {
            val line = reader.readLine()
            val notEmpty = line.isNotEmpty()

            if (notEmpty) {
                val header = HttpProtocol.toHeader(line.toLowerCase())
                headers[header.first] = header.second
            }
        } while (notEmpty)

        val contentLength = headers[HttpHeader.CONTENT_LENGTH.key.toLowerCase()]?.toInt()

        return if (contentLength != null) {
            val body = (1..contentLength).map { reader.read() }.toList()
            HttpRequest(requestLine, headers, body)
        } else
            HttpRequest(requestLine, headers)
    }

}