package org.andi.com.file

import org.andi.com.protocol.ContentType
import java.nio.charset.Charset

data class RawResponse(
        val bytes: ByteArray,
        val contentType: ContentType,
        val encoding: Charset? = null
)