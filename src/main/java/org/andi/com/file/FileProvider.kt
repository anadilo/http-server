package org.andi.com.file

import org.andi.com.protocol.ContentType
import java.util.concurrent.ConcurrentHashMap

object FileProvider {

    private val classLoader = this::class.java.classLoader
    private val fileCache = ConcurrentHashMap<String, RawResponse>()

    fun readFile(root: String, path: String): RawResponse? {
        fileCache[path]?.let { return it }

        val data = classLoader.getResource("$root/$path")?.readBytes() ?: return null
        val raw = RawResponse(data, ContentType.fromFileExtension(path))

        putToCache(path, raw)
        return raw
    }

    private fun putToCache(path: String, data: RawResponse) {
        if (fileCache.containsKey(path))
            return

        fileCache[path] = data
    }

}