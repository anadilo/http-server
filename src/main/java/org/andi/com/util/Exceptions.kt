package org.andi.com.util

fun exceptionToString(e: Exception): String {
    return e.stackTrace.joinToString("\n")
}