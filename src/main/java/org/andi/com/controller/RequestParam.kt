package org.andi.com.controller

import org.andi.com.protocol.ContentType

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class RequestParam(
        val type: ParamType = ParamType.QUERY_STRING,
        val contentTypeIn: ContentType = ContentType.NONE,
        val contentTypeOut: ContentType = ContentType.NONE
)