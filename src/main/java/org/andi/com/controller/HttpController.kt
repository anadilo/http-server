package org.andi.com.controller

import org.andi.com.engine.Server
import org.andi.com.file.RawResponse
import org.andi.com.protocol.HttpRequest
import org.andi.com.util.logger
import java.lang.reflect.Method

abstract class HttpController(klass: Class<*>, pathPrefix: String = "") {

    private val logger = logger<HttpController>()

    val mappingAnnotation = RequestMapping::class.java
    val paramAnnotation = RequestMapping::class.java

    init {
        klass.methods
                .filter { m -> m.isAnnotationPresent(mappingAnnotation) }
                .forEach { m ->
                    val requestMapping = m.getAnnotation(mappingAnnotation)

                    m.parameters
                            .filter { p -> p.isAnnotationPresent(paramAnnotation) }
                            .forEach { p ->
                                val requestParam = p.getAnnotation(paramAnnotation)
                            }

                    Server.mapRequest(requestMapping, pathPrefix) {
                        r -> RawResponse(getMethodResult(m, r), requestMapping.contentTypeOut)
                    }

                    logger.info("Mapped ${klass.simpleName}.${m.name} to $requestMapping")
                }
    }

    fun getMethodResult(method: Method, request: HttpRequest): ByteArray {
        return byteArrayOf(1, 2, 3)
    }
}