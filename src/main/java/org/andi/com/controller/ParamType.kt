package org.andi.com.controller

enum class ParamType {

    QUERY_STRING,
    PATH_VARIABLE,
    BODY

}