package org.andi.com.controller

import org.andi.com.protocol.ContentType
import org.andi.com.protocol.HttpMethod

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class RequestMapping(
        val method: HttpMethod = HttpMethod.GET,
        val path: String,
        val contentTypeIn: ContentType = ContentType.NONE,
        val contentTypeOut: ContentType = ContentType.NONE
)