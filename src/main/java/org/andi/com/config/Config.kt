package org.andi.com.config

data class Config(
        val port: Int = 80,
        val soLinger: Int? = null,
        val tcpNoDelay: Boolean = false,
        val receiveBuffer: Int? = null,
        val webRoot: String = "",
        val readTimeout: Int? = null,
        val processTimeout: Int? = null,
        val responseTimeout: Int? = null,
        val logRequests: Boolean = false,
        val logResponses: Boolean = false)