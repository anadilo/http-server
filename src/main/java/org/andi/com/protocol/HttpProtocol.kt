package org.andi.com.protocol

import java.lang.RuntimeException
import java.text.SimpleDateFormat
import java.util.*

object HttpProtocol {

    const val SPACE = " "
    const val COLON = ":"
    const val QUESTION = "?"
    const val LINE_END = "\r\n"
    const val HEADER_END = "\r\n\r\n"

    val HTTP_DATE_FORMAT = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US)

    fun toRequestLine(line: String): RequestLine {
        val split = line.split(SPACE)

        if (split.size != 3)
            throw RuntimeException("Invalid request line: $line")

        val urlParts = split[1].trim().split(QUESTION)

        return RequestLine(
                method = HttpMethod.valueOf(split[0].trim()),
                resource = urlParts[0],
                httpVersion = split[2].trim(),
                queryString = if (urlParts.size == 1) null else urlParts[1])
    }

    fun toHeader(line: String): Pair<String, String> {
        val split = line.split(COLON)

        if (split.size < 2)
            throw RuntimeException("Invalid header line: $line")

        return Pair(split[0].trim(), split[1].trim())
    }
}