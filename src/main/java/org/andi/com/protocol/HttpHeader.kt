package org.andi.com.protocol

import org.andi.com.protocol.HttpProtocol
import java.util.Date

enum class HttpHeader(
        val key: String,
        val lineCreator: ((HttpResponse) -> String)? = null) {

    HOST("Host"),

    DATE("Date", {
        "Date: ${HttpProtocol.HTTP_DATE_FORMAT.format((Date()))}${HttpProtocol.LINE_END}"
    }),

    CONTENT_LENGTH("Content-Length", { r ->
        if (r.rawResponse == null)
            ""
        else
            "Content-Length: ${r.rawResponse.bytes.size}${HttpProtocol.LINE_END}"
    }),

    CONTENT_TYPE("Content-Type", { r ->
        if (r.rawResponse == null)
            ""
        else
            "Content-Type: ${r.rawResponse.contentType.line}${HttpProtocol.LINE_END}"
    }),

    AUTHORIZATION("Authorization");

}