package org.andi.com.protocol

enum class ResponseStatus(val code: Int, val reason: String) {

    STATUS_200(200, "OK"),
    STATUS_301(301, "Moved Permanently"),
    STATUS_400(400, "Bad Request"),
    STATUS_401(401, "Unauthorized"),
    STATUS_403(403, "Forbidden"),
    STATUS_404(404, "Not Found"),
    STATUS_405(405, "Method Not Allowed"),
    STATUS_500(500, "Internal Server Error"),
    STATUS_503(503, "Service Unavailable");

    fun toStatusLine(): String {
        return "$code $reason${HttpProtocol.LINE_END}"
    }
}