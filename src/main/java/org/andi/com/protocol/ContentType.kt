package org.andi.com.protocol

import java.io.File

enum class ContentType(val line: String, val fileExtensions: Array<String>) {

    NONE("", arrayOf()),

    TEXT_HTML("text/html", arrayOf("html", "html")),
    TEXT_PLAIN("text/plain", arrayOf("txt")),
    TEXT_CSS("text/css", arrayOf("css")),
    TEXT_JAVASCRIPT("text/javascript", arrayOf("js")),
    TEXT_CSV("text/csv", arrayOf("csv")),

    APPLICATION_OCTET_STREAM("application/octet-stream", arrayOf("")),
    APPLICATION_PDF("application/pdf", arrayOf("pdf")),
    APPLICATION_JSON("application/json", arrayOf("json")),
    APPLICATION_XML("application/xml", arrayOf("xml")),

    IMAGE_JPG("image/jpg", arrayOf("jpg", "jpeg")),
    IMAGE_PNG("image/png", arrayOf("png")),
    IMAGE_GIF("image/gif", arrayOf("gif")),
    IMAGE_ICO("image/vnd.microsoft.icon", arrayOf("ico"));

    companion object {
        fun fromFileExtension(path: String): ContentType {
            val p = File(path).extension.toLowerCase()
            return values().find { t -> t.fileExtensions.contains(p) } ?: APPLICATION_OCTET_STREAM
        }
    }
}