package org.andi.com.protocol

data class RequestLine(
        val method: HttpMethod,
        val resource: String,
        val httpVersion: String,
        val queryString: String? = null
)