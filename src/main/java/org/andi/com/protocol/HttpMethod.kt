package org.andi.com.protocol

enum class HttpMethod {

    GET,
    POST,
    PUT,
    DELETE,
    HEAD

}