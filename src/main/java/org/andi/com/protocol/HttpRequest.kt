package org.andi.com.protocol

data class HttpRequest(
        val requestLine: RequestLine,
        val headers: HashMap<String, String>,
        val body: List<Int>? = null,
        val timestamp: Long = System.currentTimeMillis()
)