package org.andi.com.protocol

import org.andi.com.file.RawResponse
import java.lang.Exception

data class HttpResponse(
        val responseStatus: ResponseStatus,
        val httpVersion: String = "HTTP/1.1",
        val rawResponse: RawResponse? = null,
        val exception: Exception? = null
)