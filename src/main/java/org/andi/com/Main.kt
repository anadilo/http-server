package org.andi.com

import org.andi.com.config.Config
import org.andi.com.engine.Server
import org.andi.com.file.RawResponse
import org.andi.com.mapping.Mapping
import org.andi.com.protocol.ContentType
import org.andi.com.protocol.HttpMethod

fun main() {
    Server.start(Config(webRoot = "static", logRequests = true))

    Server.mapFile(Mapping("/"), "index.html")
    Server.mapFile(Mapping("/test"), "test.txt")
    Server.mapFile(Mapping("/favicon.ico"), "favicon.ico")

    Server.mapRequest(Mapping("/api/submit", HttpMethod.POST)) {
        httpRequest -> RawResponse("${httpRequest.body?.size ?: 0} bytes received".toByteArray(),
            ContentType.TEXT_PLAIN)
    }

}