package org.andi.com.mapping

import org.andi.com.file.RawResponse
import org.andi.com.protocol.HttpRequest

object MappingHandler {

    private val mappings = HashMap<Mapping, (HttpRequest) -> RawResponse?>()

    fun put(mapping: Mapping, action: (HttpRequest) -> RawResponse?) {
        mappings[mapping] = action
    }

    fun get(mapping: Mapping): ((HttpRequest) -> RawResponse?)? {
        return mappings[mapping]
    }

}