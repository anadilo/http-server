package org.andi.com.mapping

import org.andi.com.protocol.HttpMethod

data class Mapping(
        val resource: String,
        val method: HttpMethod = HttpMethod.GET
)