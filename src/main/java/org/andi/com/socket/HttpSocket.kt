package org.andi.com.socket

import java.io.*
import java.net.Socket

data class HttpSocket(val socket: Socket): Closeable {

    val reader = BufferedReader(InputStreamReader(socket.getInputStream()))
    val writer = BufferedOutputStream(socket.getOutputStream())

    override fun close() {
        reader.close()
        writer.close()
        socket.close()
    }
}